<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





	Route::middleware(['auth'])->group(function(){

		Route::get('/home', function(){
			return redirect()->route('room', ['room_id' => 1]);
		})->name('home');

		Route::get('/room', function(){
			return redirect()->route('room', ['room_id' => 1]);
		}); 

		Route::get('/edit_settings', 'AccountInformation@view_edit')->name('edit_page');

		Route::post('/edit', 'AccountInformation@validator')->name('validator');

		Route::post('/event', 'EventController@events')->name('eventTrigger');

		Route::get('/view_event/{room_id}', 'EventController@display_events')->name('display_events');

		Route::post('/drag_data', 'EventController@drag')->name('dragdata');

		Route::get('/room/{room_id}', 'EventController@room')->name('room');

		Route::get('/reservation', 'ReservationController@list')->name('reservation_list');

		Route::get('/add_rooms', 'EventController@add_rooms')->name('add_rooms');

		Route::post('/add_room', 'EventController@add_room')->name('add_room');

		Route::get('/approval_viewing', 'EventController@approval_viewing')->name('approval_viewing');

		Route::post('/approval', 'EventController@approval')->name('events_approval');

		Route::get('/disapproved/{id}', 'EventController@disapproved')->name('disapproved');

		Route::get('/approved/{id}', 'EventController@approved')->name('approved');
		//admin
		Route::get('/reservation_view', 'EventController@reservation_view')->name('reservation_view');
		//tl
		Route::get('/reservation_staff', 'EventController@reservation_staff')->name('reservation_view');

		Route::get('/reservation_cancel/{id}', 'EventController@cancel_for_admin')->name('cancel_admin');

		Route::get('/reservation_cancel_staff/{id}', 'EventController@cancel_for_staff')->name('cancel_admin');

		Route::post('/event/edit', 'EventController@event_edit')->name('eventEdit');

		Route::get('/cancel/{id}', 'EventController@cancel')->name('cancel_reserve');

		Route::post('/remarks', 'EventController@remarks')->name('remarks');

		Route::get('/reservation_history', 'EventController@history')->name('history');

	});

	
	Route::get('/', function () {
		return redirect()->route('viewing_room', ['room_id' => 1]);
	});

	Route::get('/viewing/{room_id}', 'ViewingController@viewing')->name('viewing_room');

	Route::get('/room_viewing/{room_id}', 'ViewingController@viewing_sched');

	Route::get('/test_route', 'TestController@testing');
	
	
	//Route::get('/rf',  'RfController@test')->name('rf_index');
	

	//Route::middleware(['auth'])->group(function(){

	Route::middleware(['auth'])->prefix('rf')->group(function () {
		Route::get('/',['as' => 'rf_index','uses' => 'RfController@test']);
		Route::get('/register-rf',['as' => 'register_rf_index','uses' => 'RfController@register_rf_view']);
		Route::get('/logged-list',['as' => 'logged-list','uses' => 'RfController@logged_view']);
		Route::post('/register-save',['as' => 'rf_save','uses' => 'ApiController@register_rf_save']);
		Route::post('/save_health_form',['as' => 'rf.save_health_form','uses' => 'RfController@save_health_form']);
		
	});
	
	

	Route::prefix('api')->group(function () {
		Route::get('/user_details',['as' => 'api.get_user_detail','uses' => 'ApiController@getUserinfo']);
		Route::get('/get_user_rfid',['as' => 'api.get_user_rfid','uses' => 'ApiController@get_user_rfid']);
		
	});
	


	Auth::routes();
	
	
	
	






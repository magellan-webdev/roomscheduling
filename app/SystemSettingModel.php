<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSettingModel extends Model
{
    protected $table = 'system_setting';
    protected $fillers = ['id', 'key','value', 'created_at', 'created_by', 'tag_deleted'];
}

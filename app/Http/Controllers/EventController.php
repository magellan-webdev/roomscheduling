<?php

namespace App\Http\Controllers;

use DB;

use App\EventModel;

use App\RoomModel;

use App\UserModel;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Redirect;

use Helper;

class EventController extends Controller
{
    public function events(Request $request){

        date_default_timezone_set('Asia/Manila');
    	$event_title = $request->input('event_title');
    	$event_date_start = $request->input('event_date_start');
        $event_date_end = $request->input('event_date_end');
        $room_id = $request->input('current_room');
        $created_at = date("Y-m-d h:i");
        $created_by = Auth::user()->emp_code;
        $model = new EventModel;
        $events = EventModel::whereBetween('start', [$event_date_start, $event_date_end])->where('room_id', '=', $room_id)->where('tag_deleted',0)->get();
        if($events->isEmpty()){
            $event_array = array('title' => $event_title,
                                     'start' => $event_date_start,
                                     'end' => $event_date_end,
                                     'created_by' => $created_by,
                                     'created_at' => $created_at,
                                     'room_id' => $room_id,
                                     'approved_by' => $created_by,
                                     'status' => '0'
                                    );   
            DB::table('evets')->insert($event_array);
            return back()->withErrors(['Succesfully Reserved', 'Error']);
        }else{
            return back()->withErrors(['Date Occupied', 'Error1']);        
        }
    	
    }

    public function display_events(Request $request, $room_id){

        $get_data = array(array());
                     
        $event = new EventModel;
       
		$helper_pu = new \Helper; 
		$toggle_approval = $helper_pu->get_toggle_approval(); 	
			
		if($toggle_approval->value == 'true')
		{
		$records = $event::where('room_id', '=', $room_id)->where('status', '=', 1)->where('tag_deleted',0)->get();
		}
		else
		{
		$records = $event::where('room_id', '=', $room_id)->where('tag_deleted',0)->get();
		}
		
        foreach($records as $record => $key){

            $data_events = array('title' => $key->title.' Reserved By: '.$key->get_information3->name,
                                 'description' => 'testing',
                                 'start' => $key->start,
                                 'color' => $color, 
                                 'end' => $key->end,
                                 'id' => $key->id,
                                 'groupId' => $emp_code
                                );

            array_push($get_data,  $data_events);
        }
        
        return response()->json($get_data);
    }

    public function room($room_id){

        $model = new RoomModel;

        $event = new EventModel;

        $users = new UserModel;

        $room_all = $model::where('tag_deleted',0)->get();

        $get_user_id = Auth::user()->id;

        $event = $event::where('room_id', '=', $room_id)->where('tag_deleted',0)->get();

        $current_room = $model::where('room_id', '=', $room_id)->where('tag_deleted',0)->first();

        $is_rep = $users::where('id', '=', $get_user_id);

        return view('home', compact('room_all', 'current_room', 'event', 'is_rep'));
    }

    public function add_rooms(){
        return view('add_rooms');
    }

    public function add_room(Request $request){
        $room_name = $request->input('room_name');
        $color = $request->input('color');
        $created_at = date("Y-m-d h:i");
        $created_by = Auth::user()->emp_code;
        $updated_by = Auth::user()->emp_code;
        $rooms = array('room_name' => $room_name,
                       'site' => '1',
                       'created_at' => $created_at,
                       'created_by' => $created_by,
                       'tagged_deleted' => '0',
                       'background_color' => $color,
                       'font_color' => 'white'
        );   
        DB::table('rooms')->insert($rooms); 

        return back();
    }

    public function approval_viewing(){
        $eventss = new EventModel;
        $room_get_unapproved = $eventss::where('status', '=', 0)->where('tag_deleted',0)->get();
        return view('approval', compact('room_get_unapproved'));
    }

    public function disapproved($id){
        $event = new EventModel;
        $get_id = $event::where('id', '=', $id)->first();
		$get_id->tag_deleted = 1;
		$get_id->updated_by = Auth::user()->emp_code;
        $get_id->save();

        return redirect()->back()->with('success', 'Reservation not Approved');
    }

    public function approved($id){
        $event = new EventModel;

        $created_by = Auth::user()->emp_code;
        $get_id = $event::where('id', '=', $id)->where('tag_deleted',0)->update(['approved_by' => $created_by,
                                                         'status' => 1]);
        return redirect()->back()->with('success_approved', 'Succesfully Approved');
    }

    public function reservation_view(){
        return view('reservation');
    }

    public function reservation_staff(){
        return view('reservation_staff');
    }

    public function cancel_for_admin($id){
        $event = new EventModel;

        $cancel_reservation = $event::where('id', '=', $id)->where('tag_deleted',0)->first();
        $cancel_reservation->tag_deleted = 1;
        $cancel_reservation->updated_by = Auth::user()->emp_code;
		$cancel_reservation->save();
		return redirect()->back()->with('cancel', 'Reservation is Cancel');
    }

    public function cancel_for_staff($id){

        $event = new EventModel;
        $color = "red";
        $cancel_reservation = $event::where('id', '=', $id)->where('tag_deleted',0)->first();
        $cancel_reservation->tag_deleted = 1;
        $cancel_reservation->updated_by = Auth::user()->emp_code;
        $cancel_reservation->remarks = "Cancelled";
        $cancel_reservation->color_text = $color;
		$cancel_reservation->save();
		

        return redirect()->back()->with('cancel', 'Your Reservation is Cancel');


    }

    /*public function event_edit(Request $request){
        
        date_default_timezone_set('Asia/Manila');
        $event_title = $request->input('event_title_edit');
        $event_date_start = $request->input('date_start_edit');
        $event_date_end = $request->input('date_end_edit');
        $event_id_edit = $request->input('event_id_edit');
        $created_at = date("Y-m-d h:i");
        $created_by = Auth::user()->emp_code;
        $model = new EventModel;
        $events = EventModel::whereBetween('start', [$event_date_start, $event_date_end])->where('room_id', '=', $room_id)->where('tag_deleted',0)->get();
        if($events->isEmpty()){
            $event_array = array('title' => $event_title,
                                     'start' => $event_date_start,
                                     'end' => $event_date_end,
                                     'created_by' => $created_by,
                                     'created_at' => $created_at,
                                     'room_id' => $room_id,
                                     'approved_by' => $created_by,
                                     'status' => '0'
                                    );   
            //DB::table('evets')->update($event_array);
            $event = new EventModel;
            $event_record = $event::where('id', '=', $event_id_edit)->where('tag_deleted',0)->first();
            $event_record->title = $event_title;
            $event_record->date_start = $event_date_start;
            $event_record->date_end = $event_date_end;
            $event_record->updated_by = Auth::user()->emp_code;
            $event_record->save();

            return back()->withErrors(['Your Reservation is Pending. Please Wait for Approval', 'Error']);
        }else{
            return back()->withErrors(['Date Occupied', 'Error1']);        
        }
    }*/ 

    public function event_edit(Request $request){
        $event = new EventModel;
        $event_title = $request->input('event_title_edit');
        $date_start = $request->input('date_start_edit');
        $date_end = $request->input('date_end_edit');
        $event_id = $request->input('event_id_edit');
        $updated_by = Auth::user()->emp_code;
        $updated_at = date('Y-m-d');
        $checking_start =  strtotime($date_start);
        $checking_end =  strtotime($date_end);

        
            if($checking_end >= $checking_start){
                $validate_date = $event::whereBetween('start', [$date_start, $date_end])->whereBetween('end', [$date_start, $date_end])->where('id', '!=', $event_id)->where('tag_deleted', '=', 0)->get();

                if($validate_date->isEmpty()){
                     
                    $update = $event::where('id', '=', $event_id)->update(['title' => $event_title,
                        'start' => $date_start,
                        'end' => $date_end,
                        'updated_by' => $updated_by
                    ]);

                    return redirect()->back()->with('edit_success', 'Succesfully Edited');
                }else{
                    return redirect()->back()->with('edit_failed', 'Date Occupied');
                }
            }
            else{
                return redirect()->back()->with('edit_failed', 'Invalid date');
            }
    }

    public function cancel($id){
        $event = new EventModel;
        $cancel_reservation = $event::where('id', '=', $id)->update(['tag_deleted' => 1]);
        return redirect()->back()->with('cancel_succesful', 'Succesfully Cancelled');

    }    


    public function remarks(Request $request){
        $event = new EventModel;
        $event_id = $request->input('event_id_hidden');
        $remarks = $request->input('remarks');

        $update_remarks = $event::where('id', '=', $event_id)->update(['tag_deleted' => 1,
            'remarks' => $remarks]);
        return redirect()->back();

    }

    public function history(){
        return view('reservation_history');
    }


    /**public function all_count(){
        $event = new EventModel;
        $counter = $event::where('status', '=', 0)->get();
        return view('layouts.app', compact('counter'));
    }**/
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\EventModel;

use App\RoomModel;

class ViewingController extends Controller
{
   public function viewing($room_id){

        $model = new RoomModel;

        $event = new EventModel;

        $room_all = $model::where('tag_deleted',0)->get();

        $event = $event::where('room_id', '=', $room_id)->where('tag_deleted',0)->get();

        $current_room = $model::where('room_id', '=', $room_id)->where('tag_deleted',0)->first();

        return view('welcome', compact('room_all', 'current_room' , 'event'));
    }

    public function viewing_sched($room_id){
    	$get_data = array(array());
                     
        $event = new EventModel;

		$helper_pu = new \Helper; 
		$toggle_approval = $helper_pu->get_toggle_approval(); 	
			
		if($toggle_approval->value == 'true')
		{
		$records = $event::where('room_id', '=', $room_id)->where('status', '=', 1)->where('tag_deleted',0)->get();
		}
		else
		{
		$records = $event::where('room_id', '=', $room_id)->where('tag_deleted',0)->get();
		}

        //$records = $event::where('room_id', '=', $room_id)->where('status', '=', 1)->where('tag_deleted',0)->get();

        foreach($records as $record => $key){
            $data_events = array('title' => $key->title.' Reserved By: '. $key->fetch_information->name,
                                 'start' => $key->start,
                                 'end' => $key->end
                                 ); 
            array_push($get_data,  $data_events);
        }
        
        return response()->json($get_data);
    }
}
 
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\Auth;

class RfController extends Controller
{
   public function test(){
		
		$GarnetAnnouncements = new \App\Http\Models\GarnetAnnouncements;
		$var1 = "";
		$latest_announcement = $GarnetAnnouncements->where('destination','=','all')->orderBy('created_at','desc')->first();
	
		return view('rfaccess.home', compact(['var1','latest_announcement']));
    }
	
	
	
	   public function register_rf_view(){
		$GarnetAnnouncements = new \App\Http\Models\GarnetAnnouncements;
		$var1 = "";
		$latest_announcement = $GarnetAnnouncements->where('destination','=','all')->orderBy('created_at','desc')->first();
	
		return view('rfaccess.register', compact(['var1','latest_announcement']));
    }
	
	
	public function logged_view(){

		$Daily_Health_Record = new \App\Http\Models\Daily_Health_Record;
		
		$var1 = "";
		$logged_list = $Daily_Health_Record->where('tag_deleted',0)->orderBy('id', 'DESC')->paginate(500);
	
		return view('rfaccess.logged_list', compact(['var1','logged_list']));
    }

   
   
   
   public function save_health_form(Request $request){
		
		
		$request->form_entry ; 
		$pieces = explode("&", $request->form_entry);
		
		$Daily_Health_Record = new \App\Http\Models\Daily_Health_Record;
		
		$Daily_Health_Record->rfid = $request->rfid;
		$Daily_Health_Record->sore_throat = $request->travelled_manila;
		$Daily_Health_Record->body_pain = $request->body_pain;
		$Daily_Health_Record->headache = $request->headache;
		$Daily_Health_Record->fever = $request->fever;
		$Daily_Health_Record->stayed = $request->stayed;
		$Daily_Health_Record->contact_with = $request->contact_with;
		$Daily_Health_Record->travelled_outside = $request->travelled_outside;
		$Daily_Health_Record->travelled_manila = $request->travelled_manila;
		
		$Daily_Health_Record->detail_emp_code = $request->detail_emp_code;
		$Daily_Health_Record->detail_name = $request->detail_name;
		$Daily_Health_Record->detail_status = $request->visitor_form;
		
		$Daily_Health_Record->detail_contact = $request->detail_contact;
		$Daily_Health_Record->detail_address = $request->detail_address;

		
		$Daily_Health_Record->logged_at = $request->logged_at;
		$Daily_Health_Record->created_by_name = Auth::user()->name;
		$Daily_Health_Record->created_by = Auth::user()->emp_code;
		$Daily_Health_Record->updated_by_name = Auth::user()->name;
		$Daily_Health_Record->updated_by = Auth::user()->emp_code;
		
		$Daily_Health_Record->save();
		
		$return_data = array();	
		$return_data['data'] =  $Daily_Health_Record;
		$return_data['msg'] = "Success";
		
		if(!$Daily_Health_Record)
		$return_data['query_result'] = 0;
		else{
		$return_data['query_result'] = 1;
		}
		
		
		 
		return response()->json($return_data);
	   
	}
   
   	
   

   
}
 
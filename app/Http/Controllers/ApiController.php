<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Auth;


use App\User;

class ApiController extends Controller
{
   public function test(){
		
		$var1 = "";
	
		return view('rfaccess.home', compact('var1'));
    }

   
   public function getUserinfo(Request $request){
   
		//$rfid_listing_model = new \App\Http\Models\Rfid_Listing;		
		//$request->rfid;
		//$rfid_listing = $rfid_listing_model->where('tag_deleted',0)->where('rfid','=',$request->rfid)->first();
		
		
		$user_model = New User;
		if($request->reference == "rfid"){
		//$rfid_listing = $rfid_listing_model->where('tag_deleted',0)->where('rfid','=',$request->rfid)-->first();
		//$user_details = $user_model->where('emp_code','=',$rfid_listing->emp_code)->with(['get_user_info'])->first();
		}
		else if ($request->reference == "emp_code")
		{
		$user_details = $user_model->where('emp_code','=',$request->emp_code)->with(['get_user_info'])->first();
		}
		
		$return_data = array();	
		$return_data['data'] =  $user_details;
		if(!$user_details)
		$return_data['query_result'] = 0;
		else{
		$return_data['query_result'] = 1;
		}
		$return_data['msg'] = "Success";
		
		
		 
		return response()->json($return_data);
	   
	}


	public function get_user_rfid(Request $request)
	{
	
		$rfid_listing_model = new \App\Http\Models\Rfid_Listing;
		if($request->reference == 'emp_code')
		{
		$rfid_listing = $rfid_listing_model->where('emp_code','=',$request->emp_code)->where('tag_deleted',0)->with(['fetch_information','fetch_information_details'])->first();
		}
		if ($request->reference == 'rfid')
		{
		$rfid_listing = $rfid_listing_model->where('rfid','=',$request->rfid)->where('tag_deleted',0)->with(['fetch_information','fetch_information_details'])->first();	
		//$rfid_listing = $rfid_listing_model->where('rfid','=','3')->where('tag_deleted',0)->first();	
		}
		$return_data = array();	
		$return_data['data'] =  $rfid_listing;
		
		if(!$rfid_listing)
		$return_data['query_result'] = 0;
		else{
		$return_data['query_result'] = 1;
		}
		$return_data['msg'] = "Success";
		
		return response()->json($return_data);
	
	}


   public function register_rf_save(Request $request){
   
		$user_model = New User;
		$user_details = $user_model->where('emp_code','=',$request->emp_code)->with(['get_user_info'])->first();
   
		$rfid_listing = new \App\Http\Models\Rfid_Listing;
		
		$rfid_listing->rfid = $request->rfid;
		$rfid_listing->user_id = $user_details->id;
		$rfid_listing->emp_code = $request->emp_code;
		$rfid_listing->name = $user_details->name;
		$rfid_listing->created_by_name = Auth::user()->name;
		$rfid_listing->created_by = Auth::user()->emp_code;
		$rfid_listing->updated_by_name = Auth::user()->name;
		$rfid_listing->updated_by = Auth::user()->emp_code;
	
		$rfid_listing->save();
		
		
		
		$return_data = array();	
		$return_data['data'] =  $rfid_listing;
		$return_data['msg'] = "Success";
		
		
		
		 
		return response()->json($return_data);
	   
	}
   
   	
   
   
   
}
 
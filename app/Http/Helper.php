<?php
namespace App\Http;

use App\EventModel;

use App\SystemSettingModel;
use App\WebAppModel;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Redirect;

class Helper{

	public static function get_count(){
		$event = new EventModel;
		$get_count = $event::where('status', '=', 0)->where('tag_deleted',0)->count();

		return $get_count;

	}

	public static function get_admin_list(){
		$event = new EventModel;
		$get_all_reservation = $event::where('status', '=', 1)->where('tag_deleted',0)->get();

		return $get_all_reservation;
	}

	public static function get_staff_list(){
		$event = new EventModel;
		$employee_code = Auth::user()->emp_code;
		$get_all_reservation = $event::where('created_by', '=', $employee_code)->where('tag_deleted',0)->get();

		return $get_all_reservation;
	}

	
	public static function get_toggle_approval(){
		$event = new SystemSettingModel;
		$get_all_reservation = $event::where('key', '=', 'toggle_approval')->where('tag_deleted',0)->first();

		return $get_all_reservation;
	}

	public static function get_history(){
		$event = new EventModel;
		$username = Auth::user()->username;
		$get_all_history = $event::where('tag_deleted', '=', 1)->where('created_by', '=', $username)->get();

		return $get_all_history;
	}


	public static function get_default_webapp(){
		$webapp_model = new WebAppModel;
		$webapp_all_default = $webapp_model::where('tag_default',0)->where('tag_deleted',0)->get();

		return $webapp_all_default;
	}


	public function get_information4(){
        return $this->hasOne('App\UserModel', 'emp_code', 'created_by');
    }

}

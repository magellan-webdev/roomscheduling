<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebAppModel extends Model
{
	protected $connection = 'gemstone';
    protected $table = 'web_app';
    protected $fillers = ['name', 'descriptions','images','url_link','link_target', 'created_at', 'updated_at', 'emp_code'];


}

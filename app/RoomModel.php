<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomModel extends Model
{
    protected $table = 'rooms';
    protected $fillers = ['room_name', 'site', 'created_at', 'created_by', 'tag_deleted'];
}

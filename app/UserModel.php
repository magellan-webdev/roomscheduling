<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
	protected $connection = 'gemstone';
    protected $table = 'users';
    protected $fillers = ['name', 'email', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at', 'emp_code'];


}

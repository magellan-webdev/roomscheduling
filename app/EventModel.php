<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EventModel extends Model
{

    protected $table = 'evets';
    protected $fillable = ['title', 'start', 'end', 'created_by', 'created_at', 'room_id', 'approved_by', 'status', 'remarks'];
    public $timestamps = false;

    public function fetch_information(){
    	return $this->hasOne('App\UserModel', 'emp_code', 'created_by');
    }

    public function get_information2(){
    	return $this->hasOne('App\UserModel', 'emp_code', 'created_by');
    }

    public function get_information3(){
        return $this->hasOne('App\UserModel', 'emp_code', 'created_by');
    }

    public function get_information4(){
        return $this->hasOne('App\UserModel', 'emp_code', 'created_by');
    }
}

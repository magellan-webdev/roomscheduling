@extends('layouts.app')

@section('content')
<div class="container">  
  <form method="post" action="{{ route('add_room') }}">
    @csrf
    <div class="form-group">
      <label for="exampleFormControlInput1">Room name</label>
      <input type="text" class="form-control" name="room_name" id="exampleFormControlInput1" placeholder="Enter Room Name">
    </div>
    
    <div class="form-group">
      <label for="exampleFormControlSelect2">Choose Color</label>
      <select class="form-control" name="color" id="exampleFormControlSelect2">
        <option value="blue">Blue</option>
        <option value="green">Green</option>
        <option value="pink">Pink</option>
        <option value="orange">Orange</option>
        <option value="indigo">Indigo</option>
      </select>
    </div>
    <div class="form-group">
      <button class="btn btn-info">Add Room</button>
      <a href="/home" class="btn btn-success">Cancel</a>
    </div>
</form>
    

  
</div>

@endsection
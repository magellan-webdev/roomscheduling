@extends('layouts.app')

@section('content')

<div class="container">
	@if(session()->has('success'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <strong>{{ session()->get('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
	    	</div>
	@endif

	@if(session()->has('success_approved'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <strong>{{ session()->get('success_approved') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
	    	</div>
	@endif
	<a href="/home">Return to home</a>
	<table class="table table-striped table-responsive-md btn-table">
	  <thead>
	    <tr>
	      <th>Purpose</th>
	      <th>Start</th>
	      <th>End</th>
	      <th>Reserved By</th>
	      <th>Created At</th>
	      <th>Action</th>
	    </tr>
	  </thead>

	  <tbody>
	@foreach($room_get_unapproved as $unapproved)  	
	    <tr>
	      <th scope="row">{{ $unapproved->title }}</th>
	      <td>{{ $unapproved->start }}</td>
	      <td>{{ $unapproved->end }}</td>
	      <td>{{ ucwords($unapproved->get_information2->name) }}</td>
	      <td>{{ $unapproved->created_at }}</td>
	      <td><a href="/approved/{{ $unapproved->id }}"><i class="fa fa-check fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;<span onclick="reject_reservation('{{ $unapproved->id }}')"><i class="fa fa-close fa-2x" aria-hidden="true"></i></span></td>
	    </tr>
	@endforeach
	  </tbody>
	</table>
</div>

<script>
      document.addEventListener('DOMContentLoaded', function() {
        setTimeout(function() {
            $(".alert").alert('close');
        }, 3000);
      });

      function reject_reservation(id){
      	if(confirm("Are you sure you want \n to Decline Reservation?")){
      		window.location.href = "/disapproved/"+id;
      	}
      }
    </script>

@endsection
@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div id='external-events'>
          <h4>Room Types</h4>
          <div id='external-events-list'>
        @foreach($room_all as $room1)    
            <a href="/room/{{ $room1->room_id }}" class='btn btn-block fc-event btn-dark {{ (request()->is("room/$room1->room_id")) ? "active" : "" }}'>{{ ucwords($room1->room_name) }}</a>
        @endforeach    
          </div>
          
          <br>
          <br>
          <br>
          <ul class="Legend">
            <p><strong>Legend</strong></p>
            <li class="Legend-item">
              <span class="Legend-colorBox" style="background-color: #28a745;">
              </span>
              <span class="Legend-label">
                You
              </span>
            </li>
            <li class="Legend-item">
              <span class="Legend-colorBox" style="background-color: #3788D8;">
              </span>
              <span class="Legend-label">
                Other
              </span>
            </li>
          </ul>
        </div>



        
        
        <div class="col-md-8">
        @if($errors->any() == "Error")  
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{ $errors->first() }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
          </div>
        @elseif($errors->any() == "Error1")
          <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>{{ $errors->first() }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
          </div> 
        @endif

            <div class="card">
              @if(session()->has('edit_success'))
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <strong>{{ session()->get('edit_success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
              @endif
                <div class="card-header" style="background-color: {{ $current_room->background_color }}; color: {{ $current_room->font_color }}"><b>{{ ucwords($current_room->room_name) }}</b></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif 
                    <div id='calendar'></div>
                </div>
                <br>
            @if(Auth::user()->is_representative == 0)    
                <button data-toggle="modal" data-target="#exampleModal" class="btn btn-info" value="">Add Event</button>
            @else    
                <p style="color:red;" class="text-center">You are not Allowed to Add Events</p>
            @endif     
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Planner --</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('eventTrigger') }}">
          @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Purpose:</label>
            <input type="text" class="form-control" name="event_title" id="recipient-name" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Date Start</label>
            <input type="datetime-local" class="datee form-control" name="event_date_start" id="date_start"  required>
          </div>
          <div class="form-group"> 
            <label for="recipient-name" class="col-form-label">Date End</label>
            <input type="datetime-local" class="datee form-control" name="event_date_end" id="date_end" required>
          </div>
          <div class="form-group">
            <input type="hidden" class="form-control" name="current_room" value="{{ $current_room->room_id }}" id="recipient-name" required>
          </div>
        </div>
   
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <!--<button type="button" id="" name="submit" class="btn btn-primary">Submit Plan</button>-->
          <input type="submit" class="btn btn-success" name="submit">
        </div>
      
      
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Planner --</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('eventEdit') }}">
          @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Event Title</label>
            <input type="text" class="form-control" name="event_title_edit" id="title_edit" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Date Start</label>
            <input type="datetime-local" class="datee form-control" name="date_start_edit" id="date_start_edit"  required>
          </div>
          <div class="form-group"> 
            <label for="recipient-name" class="col-form-label">Date End</label>
            <input type="datetime-local" class="datee form-control" name="date_end_edit" id="date_end_edit" required>
          </div>

          <div class="form-group">
            <input type="hidden" class="form-control" name="current_room_edit" value="{{ $current_room->room_id }}" id="room_id_edit" required>

            <input type="hidden" class="form-control" name="event_id_edit" value="" id="event_id_edit" required>
          </div>
        </div>
        
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" name="submit">
          @if(Auth::user()->user_type == 'administrator')
          <a class="float-left btn btn-danger"   id="cancel" href="#" data-toggle="modal" data-target="#exampleModal1">Cancel Reservation</a>
          @else

          @endif
          
          <!--<button type="button" id="" name="submit" class="btn btn-primary">Submit Plan</button>-->
          
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        

      
      
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remarks</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('remarks') }}">
          @csrf
          
          <div class="form-group">
            <label for="message-text" class="col-form-label">Remarks:</label>
            <textarea class="form-control" name="remarks" id="remarks"></textarea>
            <input type="hidden" id="event_id_remarks" name="event_id_hidden">
          </div>

       
      </div>
      <div class="modal-footer">
        <button type="submit" id="submit_remarks" class="btn btn-danger">Submit</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </form>  
    </div>
  </div>
</div>

<script>
      document.addEventListener('DOMContentLoaded', function() {
        setTimeout(function() {
            $(".alert").alert('close');
        }, 3000);

        var calendarEl = document.getElementById('calendar');

        var user = <?= Auth::user()->is_representative ?>;

        var username = <?= Auth::user()->username ?>;

        
        //var event_id = info.event.id;


        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid', 'interaction', 'timeGrid', 'list' ],
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'timeGridWeek,dayGridMonth,timeGridDay,listMonth'
          },
          defaultView: 'timeGridWeek',
          defaultDate: '<?php echo date('Y-m-d')?>',
        
          
          //defaultView: 'basicWeek',
          navLinks: true, // can click day/week names to navigate views
          businessHours: true, // display business hours
          editable: false,
          events: '/view_event/{{ $current_room->room_id }}',
          eventClick: function(info) {
            if(info.event.groupId == username){
              var search = info.event.title.substring(0, info.event.title.indexOf('|'));

              var start_new_format = moment(info.event.start).format("YYYY-MM-DDTHH:mm");
              var end_new_format = moment(info.event.end).format("YYYY-MM-DDTHH:mm");
              var date_split = start_new_format.split(" ");
              $('#title_edit').val(search);
              $('#date_start_edit').val(start_new_format);
              $('#date_end_edit').val(end_new_format);
              $('#event_id_edit').val(info.event.id);
              $('#cancel').attr('href', '/cancel/'+info.event.id);
              $('#event_id_remarks').val(info.event.id);
              $('#EditModal').modal('show');
            }
          },
          dateClick: function(info) {
            if(user != 1){
              var get_date = info.dateStr;
              var length = get_date.length;

              if(length != 10){
                $('#date_start').val(info.dateStr.substring(0, 19));
                $('#date_end').val(info.dateStr.substring(0, 19));
                $('#exampleModal').modal('show');
              }else{
                $('#date_start').val(info.dateStr+'T07:00');
                $('#date_end').val(info.dateStr+'T08:00');
                $('#exampleModal').modal('show');
              }
            }
            else{
              alert('You are not able to add Events');
            }  
            //alert(info.dateStr);
          }

          
        });
        calendar.render();
      });
    </script>

    @if(session()->has('edit_failed'))
        <script>
            alert("{{ session()->get('edit_failed') }}");
        </script>        
    @endif
@endsection

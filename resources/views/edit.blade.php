@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
          <a href="{{ route('home') }}">Go back to home</a>
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                     <h1> </h1>
                    
                    <div class="container">
                        @if($errors->any())
                          <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $error)
                              {{ $error }}
                            @endforeach
                          </div>
                        @endif
                        <form action="{{ route('validator') }}" method="post" enctype="multipart/form-data">
                          @csrf
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Change Profile</label>
                            <input type="file" class="form-control-file" name="profile" id="exampleFormControlFile1">
                          </div>

                          <div class="form-group">
                            <label for="product">Fullname</label>
                            <input type="text" class="form-control" name="fullname" id="productname" aria-describedby="emailHelp" placeholder="Enter Fullname" required>
                          </div>

                          <div class="form-group">
                            <label for="product">Email</label>
                            <input type="email" class="form-control" name="email" id="productname" aria-describedby="emailHelp" placeholder="Enter Email" required>
                          </div>

                          <div class="form-group">
                            <label for="productcategory">Change Password</label>
                            <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Enter Password" required>
                          </div>

                          <div class="form-group">
                            <label for="productcategory">Confirm Password</label>
                            <input type="password" class="form-control" name="con_password" id="exampleInputPassword1" placeholder="Confirm Password" required>
                          </div>
                         
                          <button type="submit" class="btn btn-primary">Edit Information</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


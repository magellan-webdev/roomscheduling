@extends('layouts.blank')

@section('content')
<?php $Helper = new \Helper; ?>
<div class="container-fluid">
    <div class="row justify-content-center">

        <div id='external-events'>
          <h4>Room Types</h4>

          <div id='external-events-list'>
          @foreach($room_all as $list)
            <a href="/viewing/{{ $list->room_id }}" class='btn btn-block fc-event btn-dark {{ (request()->is("room/$list->room_id")) ? "active" : "" }}'> {{ ucwords($list->room_name) }} </a>
          @endforeach
          </div>

          
        </div>

        <div class="col-md-7">
            

            <div class="card">
                
               
                <div class="card-header" style="background-color: {{ $current_room->background_color }}; color: {{ $current_room->font_color }}"><b>{{ ucwords($current_room->room_name) }}</b></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                    <div id='calendar'></div>


                </div>
                <br>
                
            </div>

            
        </div>
    </div>
	
	
	 <div class="container text-center my-3">
				<div class="row mx-auto my-auto">
					<div id="myCarousel" class="carousel slide w-100" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<!--<div class="carousel-item py-3 active">
								<div class="row">
									<?php for($x = 1; $x<= 6; $x++) {?>
									<a>	
									<div class="col-sm-2">
										<div class="card">
										  <div class="card-body">
											<h4 class="card-title">RTA</h4>
											
											<a href="#" class="btn btn-primary">Button</a>
										  </div>
										</div>
									</div>
									</a>
									<?php } ?>
								</div>
							</div> -->
							<?php $webapp = $Helper::get_default_webapp();?>
									@foreach($webapp as $key => $value)
							@if($key % 6 == 0)		
							<div class="carousel-item py-3">
								<div class="row">
							@endif		
									
									<div class="col-sm-2">
										<div class="card">
										  <div class="card-body">
											  
											 <a href="{{$value->url_link}}" target="{{$value->link_target}}">	
											<span class="card-title">{{$value->name}}</span>
											</a>
											
										  </div>
										</div>
									</div>
									
							@if( ($key % 6 == 5) || ($key == count($webapp)))		
								</div>
							</div>
							@endif		
								
								
							@endforeach
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<a class="carousel-control-prev text-dark" href="#myCarousel" role="button" data-slide="prev">
							<span class="fa fa-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next text-dark" href="#myCarousel" role="button" data-slide="next">
							<span class="fa fa-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
</div>

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog modal-confirm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="icon-box">
          <i class="material-icons">&#xE5CD;</i>
        </div>        
        <h4 class="modal-title">Sorry!</h4> 
      </div>
      <div class="modal-body">
        <p class="text-center">Your transaction has failed. Please go back and try again.</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger btn-block" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<script>

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid', 'interaction', 'timeGrid', 'list' ],
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'timeGridWeek,dayGridMonth,timeGridDay,listMonth'
          },
          defaultView: 'timeGridWeek',
          defaultDate: '<?php echo date('Y-m-d')?>',
          eventRender: function(info) {
            var tooltip = new Tooltip(info.el, {
              title: info.event.extendedProps.description,
              placement: 'top',
              trigger: 'hover',
              container: 'body'
            });
          },
          //defaultView: 'basicWeek',
          navLinks: true, // can click day/week names to navigate views
          businessHours: true, // display business hours
          editable: false,
          events: '/room_viewing/{{ $current_room->room_id }}',
          dateClick: function(info) {
            

            alert('Please Login First');
            window.location.href='{{route("login")}}';
          }


          /**dateClick: function(info){

            $('#date_start').val(info.dateStr+'T01:00');
            $('#date_end').val(info.dateStr+'T02:00');
            $('#exampleModal').modal('show');
          }
          select: function(info)**/
          
        });




        calendar.render();
      });

    </script>
@endsection
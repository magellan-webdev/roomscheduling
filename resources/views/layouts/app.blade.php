<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<?php $Helper = new \Helper; ?>
<head>
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('/profile/noc_ysz_icon.ico') }}" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Room Scheduling</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/moment.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    


    <script src="https://kit.fontawesome.com/fef9977b52.js" crossorigin="anonymous"></script>

    <!-- Full Calendar -->
    <link href='{{ URL::asset("fullcalendar/packages/core/main.css") }}' rel='stylesheet' />
    <link href='{{ URL::asset("fullcalendar/packages/daygrid/main.css") }}' rel='stylesheet' />
    <link href='{{ URL::asset("fullcalendar/packages/timegrid/main.css") }}' rel='stylesheet' />
    

    <!-- Bago pre-->
    
    
    <link href='{{ URL::asset("fullcalendar/packages/list/main.css") }}' rel='stylesheet' />
    <script src='{{ URL::asset("fullcalendar/packages/core/main.js") }}'></script>
    <script src='{{ URL::asset("fullcalendar/packages/interaction/main.js") }}'></script>
    <script src='{{ URL::asset("fullcalendar/packages/daygrid/main.js") }}'></script>
    <script src='{{ URL::asset("fullcalendar/packages/timegrid/main.js") }}'></script>
    <script src='{{ URL::asset("fullcalendar/packages/list/main.js") }}'></script>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    

   

    <script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
    <script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script>

    <script src="https://kit.fontawesome.com/79bd017199.js" crossorigin="anonymous"></script>

    <!-- Data tal-->
    


    
    <style>
      #calendar {
        max-width: 900px;
        margin: 0 auto;
      }

      .dot {
        height: 15px;
        width: 15px;
        background-color: red;
        border-radius: 50%;
        display: inline-block;
      }

        .sec{
            position: relative;
            right: -13px;
            top:-22px;
        }

        .counter.counter-lg {
            top: -24px !important;
        }

        .Legend-colorBox {
            width: 2.0rem;
            height: 0.5rem;
            display: inline-block;
            background-color: blue;
            list-style-type: none;
        }

        .Legend-item{
            list-style-type: none;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand "  tabindex="-1" href="{{ url('/') }}">
                    Room Scheduling
                </a>
				
				<div class="dropdown">
				  <button class="btn btn-secondary dropdown-toggle" tabindex="-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					RF Health Check
				  </button>
				  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
					<a class="dropdown-item" href="{{ url('/rf') }}">Health Check Form</a>
					<a class="dropdown-item" href="{{ url('/rf/register-rf') }}">Enroll Badged</a>
					<a class="dropdown-item" href="{{ url('/rf/logged-list') }}">Logs</a>
					
				  </div>
				</div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
                

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" tabindex="-1" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            
                        @else
                
                        @if(Auth::user()->user_type == "administrator")
                            <!--<a href="{{ route('approval_viewing') }}"><button class="btn"><i class="fas fa-envelope"></i><span class="counter counter-lg">{{-- $Helper::get_count() --}}</span></button></a>-->
                        @else
                            <li class="nav-item"></li>   
                        @endif    
                        
                            
                            <li class="nav-item dropdown">
                            

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <!--<img src="{{-- URL::asset('/profile/'. trans(Auth::user()->profile_picture)) --}}" class="rounded-circle"  width="45" height="45">-->&nbsp;
                                        {{ ucwords(Auth::user()->name) }}
                                </a>
                        
                                <!--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ ucwords(Auth::user()->name) }} <span class="caret"></span>
                                </a>-->
                            
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->user_type == "administrator")
                                     <a class="dropdown-item" href="/reservation_view">
                                        {{ 'Reservation List' }}
                                    </a>
                                    <a class="dropdown-item" href="/add_rooms">
                                        {{ 'Add Room' }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                @else
                                    <a class="dropdown-item" href="/reservation_history">
                                        {{ 'History' }}
                                    </a> 
                                    <a class="dropdown-item" href="/reservation_staff">
                                        {{ 'Your Reservation' }}
                                    </a>   
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a> 
                                @endif    


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
<script type="text/javascript">
      $('#exampleModal').on('show.bs.modal', function (event) {
        $.noConflict();
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
      });

    
</script>

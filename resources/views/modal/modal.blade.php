<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Planner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('eventTrigger') }}">
          @csrf
          <div class="form-group">
            <input type="hidden" class="form-control" name="event_title" value="{{ room_id }}" id="recipient-name" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Event Title</label>
            <input type="text" class="form-control" name="event_title" id="recipient-name" required>
          </div>

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Date Start</label>
            <input type="datetime-local" class="form-control" name="event_date_start" id="recipient-name" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Date End</label>
            <input type="datetime-local" class="form-control" name="event_date_end" id="recipient-name" required>
          </div>
          
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <!--<button type="button" id="" name="submit" class="btn btn-primary">Submit Plan</button>-->
          <input type="submit" class="btn btn-success" name="submit">
        </div>
      </form>
    </div>
  </div>
</div>
@extends('layouts.app')

@section('content')
<?php $helper_pu = new \Helper; ?>
<div class="container">
	
	@if(session()->has('cancel'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <strong>{{ session()->get('cancel') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
	    	</div>
	@endif
	
	<a href="/home">Return to home</a>
	<table class="table table-striped table-responsive-md btn-table">
	  <thead>
	    <tr>
	      <th>Purpose</th>
	      <th>Start</th>
	      <th>End</th>
	      <th>Reserved By</th>
	      <th>Action</th>
	    </tr>
	  </thead>

	  <tbody>
	  @foreach($helper_pu::get_admin_list() as $list)	
	    <tr>
	      <th scope="row">{{ $list->title }}</th>
	      <td>{{ $list->start }}</td>
	      <td>{{ $list->end }}</td>
	      <td>{{ $list->created_by }}</td>
	      <!-- <td>{{ $list->created_at }}</td> -->
	      <td><span onclick="cancel_reservation('{{ $list->id }}')"><i style="color:red" class="fa fa-ban fa-2x" aria-hidden="true"></i></span></td>
	    </tr>
	  @endforeach
	  </tbody>
	</table>
</div>

	<script>
      document.addEventListener('DOMContentLoaded', function() {
        setTimeout(function() {
            $(".alert").alert('close');
        }, 3000);
      });

      function cancel_reservation(id){
      	if(confirm('Are you Sure you want \n to cancel reservation')){
      		window.location.href="/reservation_cancel/"+id;
      	}
      }

    </script>

@endsection
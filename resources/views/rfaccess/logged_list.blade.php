@extends('layouts.app_datatable')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div id='external-events'>
          <h4></h4>
    
          </div>
        </div>

        
	<div class="row" > 
        <div class="col-md-12">
  

			
			  
  

		
		<div class="card"  id="health_form_complete">
				<div class="card-header" ><b>Logs </b></div>
                <div class="card-body">
					
                    <div > 
						<table class="table datatable" id ="logged_table">
						  <thead>
							<tr>
							  <th scope="col">#</th>
							  <th scope="col">Name</th>
							 
							  <th scope="col">Address</th>
							   <th scope="col">Logged Time</th>
							</tr>
						  </thead>
						  <tbody>
							@if(!empty($logged_list))
							@foreach($logged_list as $key => $val)
							<tr>
							  <th scope="row">{{$key}}</th>
							  <td>{{$val->detail_name}}</td>
							  <td>{{$val->detail_address}}</td>
							  <td>{{$val->logged_at}}</td>
							 
							 
							</tr>
							@endforeach()
							@endif
						  </tbody>
						</table>
						@if(!empty($logged_list))
						{{ $logged_list->links() }}
					@endif
					</div>
                </div>
                <br>
       
            </div>
		
		
        </div>
		
		
        </div>
    </div>	 <!-- ROw <DIV> -->
		
  

<script>

// A $( document ).ready() block.
$( document ).ready(function() {
     //$('#logged_table').DataTable();
	
	//$("#log_rf_id").focus();
});

</script>

@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div id='external-events'>
          <h4></h4>
    
          </div>
        </div>

        
	<div class="row" > 
        <div class="col-md-7">
  

          <div class="card shadow" style="background-color: #FFFFED; width: 100%;" >
			  <div class="card-header">
				
				<h2>Latest Announcement  -- <span style="font-size:30px;font-family:Orbitron;font-weight:bold" id="txt"> </span></h2></div>
			  <div class="card-block" style="overflow:auto; height:400px;">
				<h4 class="card-title" style="padding-left:5%"><u>{{$latest_announcement->title}}</u></h4>
				<p class="card-text"><small class="lead text-uppercase" style="padding-left:5%">From: {{$latest_announcement->source}} DEPARTMENT</small></p>
				<p class="card-text"><h6 class="lead text-justify" style="line-height: 120%;font-size:85%;padding-left:5%;padding-right:5%;">Dispute/s for September 10, 2020 pay-out
							{!! ($latest_announcement->content) !!}
							
							
							</h6></p>
			  </div>
			</div>
			
			
			<div class="card" id="health_check_card">
                <div class="card-header" ><b>REGISTER BADGE</b></div>
                <div class="card-body">
					 <table class="table table-bordered table-sm">
						  <thead>
							<tr>
							  <td scope="col">Employee Number</td>
							
							  <th scope="col"><input type="text" id="emp_code" onchange="display_details()" /></th>
							  <td scope="col"><!-- <i class="fas fa-times" id="span_error_checker1_icon_wrong" aria-hidden="true"></i><i class="fas fa-check hide"  aria-hidden="true"> --><span style="font-size:75%;" id="span_error_checker1" ></span></i> <input type="hidden" id="error_checker1" name="error_checker1"></td>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							 
							  <td>RFID</td>
							  <td><input type="text" id="rfid_1" onchange="validated_rfid()"/></td>
							  <td><span style="font-size:75%;" id="span_error_checker2" ></span></i> <input type="hidden" id="error_checker2" name="error_checker2"></td>
							</tr>
							<tr>
							 
							  <td>Confirm RFID</td>
							  <td><input type="text" id="rfid_2" onchange="validated_rfid_match()"/></td>
							  <td><span style="font-size:75%;" id="span_error_checker3" ></span></i> <input type="hidden" id="error_checker3" name="error_checker3"></td>
							</tr>
							
							
							
						  </tbody>
						</table>
						
					
							
						
						
						
						
						
                </div>
            
				
				
			</div>
			
		

        </div>
		
		<div class="col-md-5">
	
						
			 <div class="card" style="display:none" id="personal_info_card">
					<div class="card-header" ><b>Personal Information</b></div>
                <div class="card-body">
					
                    <div id=''> 
					
					
					 <table class="table table-bordered">
						
						  <tbody>
							<tr>
							  <th width="22%" scope="row">Employee # </th>
							  <td><span id="detail_emp_code" class="input_detail"></span> <input type="hidden" id="detail_emp_code_input"/> </td>
							  
							</tr>
							<tr>
							  <th  scope="row">Name </th>
							  <td><span id="detail_name" class="input_detail"></span></td>
							  
							</tr>
							<tr>
							  <th scope="row">Employee</th>
							  <td><span id="detail_status" class="input_detail"></span></td>
						
							</tr>
							<tr>
							  <th scope="row">Address </th>
							  <td ><span id="detail_address" class="input_detail"></span></td>
						
							</tr>
						  </tbody>
						</table>
					
					
						
						
					</div>
                </div>
                <br>
       
            </div>
  
			<div class="card" style="display:none" id="last_register">
				<div class="card-header" ><b>Notice</b></div>
                <div class="card-body">
					
                    <div id='success_div_message'> 
			
						
						
					</div>
                </div>
                <br>
       
            </div>
  
			

            
            </div>
        </div>
    </div>	 <!-- ROw <DIV> -->
		
    </div>
</div>


<script>
 function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
    var ampm = h >= 12 ? 'PM' : 'AM';
  h = h % 12;
  document.getElementById('txt').innerHTML =
  h + ":" + m + ":" + s  + ' ' + ampm;;;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}

// A $( document ).ready() block.
$( document ).ready(function() {
    startTime();
});
function submit_record(){
	if ($("#log_rf_id_2").val() == $("#log_rf_id").val())
	{
	//alert('submit');
	}
	else
	{
	//alert($("#log_rf_id_2").val());
	}
	
}

function cleardata(paramter1){
	$("#" + paramter1).val("");
	
}


function validated_rfid()
	{	
		$("#error_checker2").val("");
		$("#span_error_checker2").html('');
		
		//alert('yesy');
		let rfid = $("#rfid_1").val();
		if(($("#rfid_2").val() != "") && ($("#rfid_1").val() != $("#rfid_2").val()))
		{
			$("#error_checker3").val('invalid')
			$("#span_error_checker3").css( "color", "red" );
			$("#span_error_checker3").html('Badge do not match');
			
		}
		if(rfid !== ""){
						$.ajax({
										url: '{{ route("api.get_user_rfid") }}',
										type: 'GET',
										data: {
											"_token": "{{ csrf_token() }}",
											//"rfid" : user_rfid,
											"reference" : "rfid",
											//"emp_code" :  rfid,
											"rfid" : rfid,
											
										},
										cache: false,
										dataType: "json",
										success:function(data){
											
											if(data){
												if(data.query_result == 0)
												{				
													
												$("#error_checker2").val("validated");
												$("#span_error_checker2").css( "color", "green" );
												$("#span_error_checker2").html('validated');
												$("#rfid_1").attr("placeholder", "");
												$( "#rfid_2" ).focus();
												}	
												else {
												$("#error_checker2").val("invalid");
												$("#span_error_checker2").css( "color", "red" );
												$("#span_error_checker2").html('Badge if already registered');
												
												$("#rfid_1").attr("placeholder", $("#rfid_1").val());
												$("#rfid_1").val("");
												//$("#rfid_1").select();
												}	
						
										   }
									   },
									   error:function(data){
										alert(error);
									   }
									});	
									
						if( ($("#error_checker2").val() == "validated") && ($("#error_checker3").val() == "validated"))  
						{		
							save_rfid();		
						}				
					}
	}



function save_rfid()
{
	$("#emp_code").attr("placeholder","");
	$("#rfid_1").attr("placeholder","");
	$("#rfid_2").attr("placeholder","");
	$("#error_checker1").val("");
	$("#span_error_checker1").html("");
	$("#error_checker2").val("");
	$("#span_error_checker2").html("");
	$("#error_checker3").val("");
	$("#span_error_checker3").html("");
	
	let emp_code = $("#emp_code").val();
	let rfid_1 = $("#rfid_1").val();
	let rfid_2 = $("#rfid_2").val();
	
	
	$.ajax({
					url: '{{ route("rf_save") }}',
					type: 'POST',
					data: {
						"_token": "{{ csrf_token() }}",
						"rfid" : rfid_1,
						"reference" : "emp_code",
						"emp_code" : emp_code,
						
					
						

					},
					cache: false,
					dataType: "json",
					success:function(data){
						
						if(data){
							
							$("#error_checker1").val('');
							$("#error_checker2").val('');
							$("#error_checker3").val('');
							$("#span_error_checker1").html("");
							$("#span_error_checker2").html("");
							$("#span_error_checker3").html("");
							$("#last_register").slideDown();
							$("#success_div_message").html(emp_code + '  Has Been Register');
							$("#emp_code").val("");
							$("#rfid_1").val("");
							$("#rfid_2").val("");
							
							$("#personal_info_card").slideUp();
							$("#emp_code").focus();
						//$("#log_rf_id_2").val(data.data.avatar );
						
						
					   }
				   },
				   error:function(data){
					alert(error);
				   }
				});		
	
	
}




function display_details(){
	$("#error_checker1").val("");
	$("#detail_emp_code_input").val("");
	$("#span_error_checker1").html("");
	
	var data1 = $("#emp_code").val();
	//var d = new Date(Date.now()).toLocaleString(); 
	if(data1 !== "" )
	{
	query_user_info(data1);
	//$("#personal_info_card").slideDown();
	$("#last_register").slideUp();
	}
	else{
	
	/// CLEAR INPUT AND PROFILE	
	$("#error_checker1").val('');
	$("#personal_info_card").slideUp();
	
	}
	
	if( ($("#error_checker2").val() == "validated") && ($("#error_checker3").val() == "validated"))  
	{		
		save_rfid();		
	}		
	
}


function validated_emp_code_if_register()
	{
	let emp_code = $("#emp_code").val();
	let detail_emp_code_input = $("#detail_emp_code_input").val();
	$.ajax({
					url: '{{ route("api.get_user_rfid") }}',
					type: 'GET',
					data: {
						"_token": "{{ csrf_token() }}",
						//"rfid" : user_rfid,
						"reference" : "emp_code",
						"emp_code" : emp_code,
						
					},
					cache: false,
					dataType: "json",
					success:function(data){
						
						if(data){
							if( (data.query_result == 0) && (detail_emp_code_input == "1"))
							{								
							$("#error_checker1").val("validated");
							$("#span_error_checker1").css( "color", "green" );
							$("#span_error_checker1").html('validated');
							$( "#rfid_1" ).focus();
							}	
							else {
							$("#error_checker1").val("invalid");
							$("#span_error_checker1").css( "color", "red" );
							$("#span_error_checker1").html('has Existing Active Badge');
							
							}	
	
					   }
				   },
				   error:function(data){
					alert(error);
				   }
				});	
	
		
	}



function query_user_info(parameter1){
			$.ajax({
					url: '{{ route("api.get_user_detail") }}',
					type: 'GET',
					data: {
						"_token": "{{ csrf_token() }}",
						//"rfid" : user_rfid,
						"reference" : "emp_code",
						"emp_code" : parameter1,
						
					
						

					},
					cache: false,
					dataType: "json",
					success:function(data){
						
						if(data){
						//$("#log_rf_id_2").val(data.data.avatar );
						if(data.query_result == 1)
						{
					
						$("#detail_emp_code").html(data.data.emp_code );
						$("#detail_name").html(data.data.name );
						$("#detail_status").html(data.data.emp_code );
						
						if(data.data.get_user_info){
						$("#detail_address").html(data.data.get_user_info.address );						
						}
						$("#detail_emp_code_input").val(data.query_result );						
						//detail_emp_code_input
						$("#personal_info_card").show();		
						validated_emp_code_if_register();			
								
						
						
						}
						else{
						$("#error_checker1").val('invalid');												
						$("#emp_code").attr("placeholder", $("#emp_code").val());
						$("#emp_code").val('');
						$("#emp_code").focus();
							
						$("#span_error_checker1").html('Invalid Employee Number');
						$("#span_error_checker1").css( "color", "red" );
						
						$("#personal_info_card").slideUp();
						
						$("#detail_emp_code_input").val(data.query_result );			
						$("#detail_emp_code").html("");
						$("#detail_name").html("");
						$("#detail_status").html("");
						$("#detail_address").html("");
						
						}
						//locPaymentList(transaction_number);
					   }
				   },
				   error:function(data){
					alert(error);
				   }
				});	
	
	
	
	
}

function validated_rfid_match()
{
	$("#rfid_2").attr("placeholder", "");
	$("#error_checker3").val("");
	$("#span_error_checker3").html('');
	if($("#rfid_2").val() !== "")
	{	
			if($("#rfid_1").val() == $("#rfid_2").val() )
			{
				$("#error_checker3").val("validated");
				
				//$("#error_checker1").val("validated");
				$("#span_error_checker3").css( "color", "green" );
				$("#span_error_checker3").html('validated');
				}			
			else
			{
				
				$("#rfid_2").attr("placeholder", $("#rfid_2").val());
				$("#rfid_2").val("");
				$("#error_checker3").val("invalid");
				$("#span_error_checker3").css( "color", "red" );
				$("#span_error_checker3").html('Badge do not match');
				
				
			}
			
			if( ($("#error_checker2").val() == "validated") && ($("#error_checker3").val() == "validated"))  
			{		
				save_rfid();		
			}
	}		
	
}


function show_info(){
	
	$("#emp_code").attr("placeholder", "");
	var data1 = $("#log_rf_id").val();
	var d = new Date(Date.now()).toLocaleString(); 
	query_user_info(data1);
	//$("#temperature").focus();
	 // set an element
     //$("#date").val( moment().format('MMM D, YYYY') );

     // set a variable
    
	
	//$("#health_check_card").show();
	//$("#log_time").val( d   );
	//no$("#log_rf_id_2").val(data1   );
	
}
</script>

@endsection

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div id='external-events'>
          <h4></h4>
    
          </div>
        </div>

        
	<div class="row" > 
        <div class="col-md-7">
  
			<div class="card shadow" style="background-color: #FFFFED; width: 100%;display:none" >
			  <div class="card-header">
				
				<h2>Latest Announcement  -- <span style="font-size:30px;font-family:Orbitron;font-weight:bold" id="txt"> </span></h2></div>
			  <div class="card-block" style="overflow:auto; height:400px;">
				<h4 class="card-title" style="padding-left:5%"><u>{{$latest_announcement->title}}</u></h4>
				<p class="card-text"><small class="lead text-uppercase" style="padding-left:5%">From: {{$latest_announcement->source}} DEPARTMENT</small></p>
				<p class="card-text"><h6 class="lead text-justify" style="line-height: 120%;font-size:85%;padding-left:5%;padding-right:5%;">{!! ($latest_announcement->content) !!}</h6></p>
			  </div>
			</div>
			
			  
  

            <div class="card">		
					<form id="Health_Check_Form" action="#" method="POST">
					
					
                <div class="card-header" ><b> <input type ="text" autocomplete="off" style="width: 90px;    border: solid 5px;
    border-color: #f50303;" onchange="show_info()" tabindex=1 onfocus="this.select()"  onclick="this.select()"  id="log_rf_id"/></b> <span style="font-size:22px">BADGE</span> <span style="float:right;    margin-top: 8px;"><span style="font-size:20px">VISITOR &nbsp;&nbsp;&nbsp;</span><input type="checkbox" style=" transform: scale(2);"  id="visitor_check_box" name="visitor_form" /></span></div>
                
              
       
            </div>
			
			<div class="card" style="display:none" id="unregister_badge">
				<div class="card-header" ><b></b></div>
                <div class="card-body">
					
                    <div id='success_div_message'> 
						<center><strong style="color:red;font-size:300%">UNREGISTER BADGE</strong></center>
						
						
					</div>
                </div>
                <br>
       
            </div>
		
		<div class="card" style="display:none" id="health_form_complete">
				<div class="card-header" ><b></b></div>
                <div class="card-body">
					
                    <div id='success_div_message2'> 
						<center><strong style="color:green;font-size:300%"><span id="complete_reg_name"></span> <br/>Complete</strong></center>
						
						
					</div>
                </div>
                <br>
       
            </div>
		
		
			
			 <div class="card" style="display:none" id="personal_info_card">
                <div class="card-header" ><b>Personal Information</b></div>
                <div class="card-body" style="padding:0 0 0 0">
					
                    <div id=''> 
					
					
					 <table class="table table-bordered">
						
						  <tbody>
							<tr id="is_employee_label" style="display:none">
							  <th width="22%" scope="row"><span >Employee # </th>
							  <td><input type="text" id="detail_emp_code" name="detail_emp_code" tabindex="-1" style="border-width:0px;border:none;" readonly class="input_detail"></td>
							  
							</tr>
							<tr>
							  <th  scope="row">Name </th>
							  <td><input type="text" id="detail_name" class="input_detail" tabindex="-1" name="detail_name" style="border-width:0px;border:none;"></td>
							  
							</tr>
							
							<tr style="display:none">
							  <th scope="row">Employee</th>
							  <td><input type="text" id="detail_status" class="input_detail"  tabindex="-1" name="detail_status" style="border-width:0px;border:none;" ></td>
						
							</tr>
							
							<tr>
							  <th scope="row">Contact #</th>
							  <td><input type="text" id="detail_contact" class="input_detail"  tabindex="-1" name="detail_contact" style="border-width:0px;border:none;width:100%;" ></td>
						
							</tr>
							
							<tr>
							  <th scope="row">Address </th>
							  <td ><input type="text" id="detail_address" class="input_detail" tabindex="-1" name="detail_address" style="border-width:0px;border:none;width:100%" ></td>
						
							</tr>
						  </tbody>
						</table>
					
					
						
						
					</div>
                </div>
              
            </div>
        </div>
		
		<div class="col-md-5">
  

            <div class="card" id="health_check_card" style="display:none">
                <div class="card-header" ><b>MAGELLAN E-SUPPORT SERVICES, INC. HEALTH CHECK FORM</b></div>
                <div class="card-body">
					<span style="float:right;font-size:30px">Temperature : <input type="text" tabindex=2 id="temperature" name="temperature" onchange="submit_temperature()" style="width:90px" maxlength="6"/></span>
                    <table class="table table-bordered table-sm">
						  <thead>
							<tr>
							  <td scope="col">1. Are You Experiencing</td>
							
							  <th scope="col">YES</th>
							  <th scope="col">NO</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							 
							  <td>Sore Throat</td>
							  <td><input type="radio" name="sore_throat" value="yes" tabindex="-1"> </td>
							  <td><input type="radio" name="sore_throat" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							
							<tr>
							 
							  <td>Body Pains</td>
							  <td><input type="radio" name="body_pain" value="yes" tabindex="-1"> </td>
							  <td><input type="radio" name="body_pain" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							
							<tr>
							 
							  <td>Headache</td>
							  <td><input type="radio" name="headache" value="yes"> </td>
							  <td><input type="radio" name="headache" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							<tr>
							 
							  <td>Fever</td>
							  <td><input type="radio" name="fever" value="yes"> </td>
							  <td><input type="radio" name="fever" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							
							
							
							<tr>
							  
							  <td>2. Have you worked together or stayed in the same close environment of a confirmed COVID-19 case?</td>
							  <td><input type="radio" name="stayed" value="yes" tabindex="-1"> </td>
							  <td><input type="radio" name="stayed" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							
							<tr>
							  
							  <td>3. Have you had any contact with anyone with fever, cough,colds, sore throat in the past 2 weeks?</td>
							  <td><input type="radio" name="contact_with" value="yes" tabindex="-1"> </td>
							  <td><input type="radio" name="contact_with" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							<tr>
							  
							  <td>Have you travelled outside the Philippines for the last 14 days?</td>
							  <td><input type="radio" name="travelled_outside" value="yes" tabindex="-1"> </td>
							  <td><input type="radio" name="travelled_outside" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							<tr>
							  
							  <td>Have you travelled to any area in NCR aside from your home</td>
							  <td><input type="radio" name="travelled_manila" value="yes" tabindex="-1"> </td>
							  <td><input type="radio" name="travelled_manila" value="no" checked="checked" tabindex="-1"> </td>
							</tr>
							
						  </tbody>
						</table>
						
						<div class="container" style="font-size:12px">
						
						I hereby authorize MAGELLAN E-SUPPORT SERVICES, INC. to collect and process the date indicated herein for the purpose of effecting control of the COVID-19 infection. I understand that my personal information is proected by RA 10173 Data Privacy Act of 2012 and I am required by RA 11469 Bayanihan to heal as One ACT to provide thruthful information
						<br/>
						<br/>
						
						
						
						</div>
						
							<div class="col-md-6" style="float:left">
								<input type="text" autocomplete="off" style="width: 90px;    border: solid 5px;
    border-color: #f50303;" tabindex=3 onchange="submit_record()"  onfocus="this.select()"  onclick="this.select()"  id="log_rf_id_2" name="rfid"><span id="error_display_3" style="display:none;color:red">Badge Did not Match</span>
								<br/>
								<br/>
							</div>	
							
							<div class="col-md-6" style="float:right">
								<span class="btn btn-primary" id="btn_visitor_submit" onclick="visitor_submit()" />submit</span>
								<input type="text" autocomplete="off" name="logged_at" id="log_time" tabindex="-1" readonly class="readonly"> 
								<br/>
								<br/>
								
								
							</div>	
						
						
						
						</form>
						
						
                </div>
            
				
				
				</div>
            </div>
        </div>
    </div>	 <!-- ROw <DIV> -->
		
    </div>
</div>


<script>
 function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);

  
  var ampm = h >= 12 ? 'PM' : 'AM';
  h = h % 12;
  
  document.getElementById('txt').innerHTML =
  h + ":" + m + ":" + s + ' ' + ampm;;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}

function hidemessage() {
  setTimeout(function(){$("#health_form_complete").slideUp(); }, 10000);
}



function submit_temperature(){
	if($("#temperature").val().length <= 5)
		{
			$("#log_rf_id_2").focus();
		}
	else{
		
		$("#temperature").attr("placeholder", $("#temperature").val());
		$("#temperature").val("");
		//$("#log_rf_id_2").focus();
		//$("#temperature").focus();
		//$("#temperature").focus();
		
	}

}

// A $( document ).ready() block.
$( document ).ready(function() {
    startTime();
	
	$("#log_rf_id").focus();
	
	
	$("#visitor_check_box").on("change", function(){
		if($('#visitor_check_box').is(":checked"))
			{
			cleardata();
			$("#personal_info_card").slideDown();
			$("#health_check_card").slideDown();
			var d = new Date(Date.now()).toLocaleString(); 
			$("#log_time").val( d   );	
			$("#is_employee_label").hide();	
			$("#btn_visitor_submit").show();
			$("#log_rf_id").val("");	
			
			}
		else{
			$("#personal_info_card").slideUp();
			$("#health_check_card").slideUp();
			$("#btn_visitor_submit").hide();
			}
		
		});
	
});


function visitor_submit(){
	
	//alert('submit test');	
	var form_entry = $( "form" ).serialize() + "&reference=rfid"  ;
	//var form_entry =  JSON.stringify( $("form").serializeArray() )
		
						$.ajax({
						url: '{{ route("rf.save_health_form") }}',
						type: 'POST',
						data: form_entry,
						
						cache: false,
						dataType: "json",
						success:function(data){
							
							if(data){
								if(data.query_result == 1)
								{				
								
								

								$("#personal_info_card").slideUp();
								$("#health_check_card").slideUp();
								$("#temperature").val("");
								$("#health_form_complete").slideDown();
								$("#complete_reg_name").html(data.data.detail_name);
								$("#log_rf_id").val('');
								$( "#log_rf_id" ).focus();
								hidemessage();
								}	
								else {
								
								}	
		
						   }
					   },
					   error:function(data){
						alert(error);
					   }
					});	
		
	
}


function submit_record(){
	$("#log_rf_id_2").attr("placeholder","");
	if($("#temperature").val() == ""){
		$("#log_rf_id_2").val("");
		$("#temperature").focus();
	}
	else if (($("#log_rf_id_2").val() == $("#log_rf_id").val()  && $("#temperature").val() != ""))
	{
	//alert('submit test');	
	var form_entry = $( "form" ).serialize() + "&reference=rfid"  ;
	//var form_entry =  JSON.stringify( $("form").serializeArray() )
		
						$.ajax({
						url: '{{ route("rf.save_health_form") }}',
						type: 'POST',
						data: form_entry,
						
						cache: false,
						dataType: "json",
						success:function(data){
							
							if(data){
								if(data.query_result == 1)
								{				
								
								

								$("#personal_info_card").slideUp();
								$("#health_check_card").slideUp();
								$("#temperature").val("");
								$("#health_form_complete").slideDown();
								$("#complete_reg_name").html(data.data.detail_name);
								$("#log_rf_id").val('');
								$( "#log_rf_id" ).focus();
								hidemessage();
								}	
								else {
								
								}	
		
						   }
					   },
					   error:function(data){
						alert(error);
					   }
					});	
		
	
	
	}
	else if($("#log_rf_id_2").val() != '')
	{
		$("#health_form_complete").slideUp();
		$("#error_display_3").slideDown();
		$("#log_rf_id_2").attr("placeholder",$("#log_rf_id_2").val());
		$("#log_rf_id_2").val("");
		$("#log_rf_id_2").focus();
	//alert($("#log_rf_id_2").val());
	}
	
}

function cleardata(){
	$("#temperature").val("");
	$("#log_rf_id_2").val("");
	$("#log_time").val("");
	$("#detail_contact").val("");
	$("#detail_status").val("");
	$("#detail_address").val("");
	$("#detail_emp_code").val("");
	$("#detail_name").val("");
	$("#temperature").attr("placeholder", "");
	
	
}







function query_user_info()
	{
		
		let rfid = $("#log_rf_id").val();
		
		$.ajax({
						url: '{{ route("api.get_user_rfid") }}',
						type: 'GET',
						data: {
							"_token": "{{ csrf_token() }}",
							//"rfid" : user_rfid,
							"reference" : "rfid",
							//"emp_code" :  rfid,
							"rfid" : rfid,
							
						},
						cache: false,
						dataType: "json",
						success:function(data){
							
							if(data){
								if(data.query_result == 0)
								{				
								
								$("#detail_emp_code").val("" );
								$("#detail_name").val("" );
								$("#detail_status").val("" );
								$("#log_time").val("");
								$("#unregister_badge").slideDown();
								$("#personal_info_card").slideUp();
								$("#health_check_card").slideUp();
								
								$("#log_rf_id").attr("placeholder",$("#log_rf_id").val());
								$("#log_rf_id").val("");
								$("#log_rf_id").focus();
								//$("#error_checker2").val("validated");
								//$("#span_error_checker2").css( "color", "green" );
								//$("#span_error_checker2").html('validated');
								
								
								}	
								else {
								var d = new Date(Date.now()).toLocaleString(); 
								$("#unregister_badge").slideUp();
								$("#personal_info_card").slideDown();
								$("#health_check_card").slideDown();
								$("#temperature").focus();
								$("#log_time").val( d   );	
								
								$("#detail_emp_code").val(data.data.emp_code );
								$("#detail_name").val(data.data.name );
								<!-- $("#detail_status").val(data.data.emp_code ); -->
								$("#detail_contact").val(data.data.fetch_information_details.contact_no );
								$("#detail_address").val(data.data.fetch_information_details.address );
				
								}	
		
						   }
					   },
					   error:function(data){
						alert(error);
					   }
					});	

	}




function show_info(){
	$("#btn_visitor_submit").slideUp();
	$("#log_rf_id").attr("placeholder","");
	$("#is_employee_label").show();
	$("#visitor_check_box").prop('checked', false); 
	$("#error_display_3").slideUp();
	$("#health_form_complete").slideUp();
	cleardata();
	var data1 = $("#log_rf_id").val();
	
	query_user_info(data1);

	 // set an element
     //$("#date").val( moment().format('MMM D, YYYY') );

     // set a variable
    
	//no$("#log_rf_id_2").val(data1   );
	
}



function visitor_form(){
	//$("#visitor_check_box").val();
	alert('yesy');
}
</script>

@endsection

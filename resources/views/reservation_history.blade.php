@extends('layouts.app')

@section('content')
<?php $helper_pu = new \Helper; ?>
<div class="container">
	
	@if(session()->has('cancel'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <strong>{{ session()->get('cancel') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
	    	</div>
	@endif
	    
	
	<a href="/home">Return to home</a>
	<table class="table table-striped table-responsive-md btn-table display" id="example1">
	  <thead>
	    <tr>
	      <th>Title</th>
	      <th>Start</th>
	      <th>End</th>
	      <th>Remarks</th>
	    </tr>
	  </thead>

	  <tbody>
	  @foreach($helper_pu::get_history() as $list)	
	    <tr>
	      <th scope="row">{{ ucwords($list->title) }}</th>
	      <td>{{ $list->start }}</td>
	      <td>{{ $list->end }}</td>
	      
	      <td style="color:{{ $list->color_text  }}"><strong>{{ $list->remarks }}</strong></td>
	      <!--<td>{{-- if($list->status == 1) --}}</td>-->
	     
	    </tr>
	  @endforeach
	  </tbody>
	</table>
</div>

	<script>
      document.addEventListener('DOMContentLoaded', function() {
        setTimeout(function() {
            $(".alert").alert('close');
        }, 3000);
      });


      function cancel_reservation(id){
      	if(confirm("Are you sure you  want \n to  cancel your  reservation?")){
      		window.location.href="/reservation_cancel_staff/"+id;
      	}

      }

      $(document).ready(function () {
            $.noConflict();
            var table = $('#example1').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bInfo": true,
                "bAutoWidth": true,
                "bPageLength": 5
            });
        });

    </script>



@endsection